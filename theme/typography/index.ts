export type { TTypographicGrid } from "./grid";
export type {
  TTypographicScaleNames,
  TTypographicScale,
  TTypographicScaleSettings,
  TTypographicScaleSettingsForLinear,
  TTypographicScaleSettingsForModular,
} from "./scale";
export type { TTypography } from "./typography";
export type { TFont, TFontNames } from "./fonts";

export { typographicGrid } from "./grid";
export { typographicScale } from "./scale";
export { defaultFont } from "./fonts";
export { typography } from "./typography";
